# bitbucket-bot

Environment variables:

* `BOT_TOKEN` - Telegram Bot API token
* `CHAT_ID` - Telegram chat ID to send messages
* `PORT` - Port to listen
