(defproject bitbucket-bot "0.1.0-SNAPSHOT"
  :main ^:skip-aot bitbucket-bot.core
  :target-path "target/%s"
  :min-lein-version "2.0.0"
  :profiles {:uberjar {:aot :all
                       :uberjar-name "bitbucket-bot.jar"}}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-json "0.4.0"]
                 [clj-http "3.9.0"]
                 [cheshire "5.8.0"]
                 [ring/ring-jetty-adapter "1.6.3"]])
