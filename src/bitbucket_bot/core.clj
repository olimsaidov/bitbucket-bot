(ns bitbucket-bot.core
  (:gen-class)
  (:require [ring.adapter.jetty :as jetty]
            [clojure.string :refer [split join]]
            [ring.middleware.json :refer [wrap-json-body]]
            [clj-http.client :as client]))


(defn env [name]
  (System/getenv name))


(defn- send-message [token params]
  (->> {:form-params  params
        :content-type :json
        :as           :json}
       (client/post (str "https://api.telegram.org/bot" token "/sendMessage"))
       (:body)))


(defn parse [payload]
  (->> payload
       :push
       :changes
       (map (fn [change]
              {:truncated (-> change :truncated)
               :action    (cond
                            (-> change :created) "created"
                            (-> change :closed) "closed"
                            (-> change :forced) "force-updated"
                            :else "updated")
               :branch    {:name (or (-> change :new :name)
                                     (-> change :old :name))
                           :link (or (-> change :new :links :html :href)
                                     (-> change :old :links :html :href))}
               :repo      {:name (-> payload :repository :name)
                           :link (-> payload :repository :links :html :href)}
               :commits   (->> change
                               :commits
                               (map (fn [commit]
                                      {:message (-> commit :message (split #"\n") first)
                                       :author  (-> commit :author :user :display_name)
                                       :link    (-> commit :links :html :href)})))}))))


(defn message [changes]
  (->> changes
       (map (fn [change]
              (format "🎈 <a href=\"%s\">%s</a>/<a href=\"%s\">%s</a> %s%s\n%s"
                      (-> change :repo :link)
                      (-> change :repo :name)
                      (-> change :branch :link)
                      (-> change :branch :name)
                      (-> change :action)
                      (if (seq (-> change :commits)) ":" "")
                      (->> change :commits
                           (map (fn [commit]
                                  (format " • %s: <a href=\"%s\">%s</a>"
                                          (-> commit :author)
                                          (-> commit :link)
                                          (-> commit :message))))
                           (join "\n")))))
       (join "\n\n")))


(defn handler [request]
  (let [text (-> request :body parse message)]
    (send-message
      (env "BOT_TOKEN")
      {:chat_id (env "CHAT_ID") :parse_mode "html" :text text})
    {:status  200
     :headers {"Content-Type" "text/html; charset=utf-8"}
     :body    text}))


(def app
  (-> handler
      (wrap-json-body {:keywords? true})))


(def server (atom nil))


(defn get-port [default]
  (try
    (Integer/parseInt (env "PORT"))
    (catch Exception _ default)))


(defn start [port]
  (reset! server
          (jetty/run-jetty #'app {:port  port
                                  :join? false})))


(defn stop []
  (.stop (deref server))
  (reset! server nil))


(defn -main
  [& args]
  (let [port (get-port 6767)]
    (start port)
    (println "Server started at port" port)))

